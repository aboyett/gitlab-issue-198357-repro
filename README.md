# gitlab-issue-198357-repro

This repo provides a simple reproduction of Gitlab issue 198357

#### Setup
`FILE_VAR` is a file variable defined in CI Variables.
`NEW_VAR` is a variable with a value of `$FILE_VAR`

Ideally `NEW_VAR` should contain a file-path, like `FILE_VAR`, in practice it
contains the value of the file, rather than the path to the file.
